package nherlianto.psaketlboii;

import java.math.BigDecimal;

public class CcfData {
    private String instrumentType;
    private String productType;
    private String contractNumber;
    private String defaultDate;
    private BigDecimal outstandingPrincipal;
    private BigDecimal outstandingPrincipalPreviousYear;
    private BigDecimal unusedLimitPreviousYear;

    public CcfData(
            String instrumentType,
            String productType,
            String contractNumber,
            String defaultDate,
            BigDecimal outstandingPrincipal,
            BigDecimal outstandingPrincipalPreviousYear,
            BigDecimal unusedLimitPreviousYear) {
        this.instrumentType = instrumentType;
        this.productType = productType;
        this.contractNumber = contractNumber;
        this.defaultDate = defaultDate;
        this.outstandingPrincipal = outstandingPrincipal;
        this.outstandingPrincipalPreviousYear = outstandingPrincipalPreviousYear;
        this.unusedLimitPreviousYear = unusedLimitPreviousYear;
    }

    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getDefaultDate() {
        return defaultDate;
    }

    public void setDefaultDate(String defaultDate) {
        this.defaultDate = defaultDate;
    }

    public BigDecimal getOutstandingPrincipal() {
        return outstandingPrincipal;
    }

    public void setOutstandingPrincipal(BigDecimal outstandingPrincipal) {
        this.outstandingPrincipal = outstandingPrincipal;
    }

    public BigDecimal getOutstandingPrincipalPreviousYear() {
        return outstandingPrincipalPreviousYear;
    }

    public void setOutstandingPrincipalPreviousYear(BigDecimal outstandingPrincipalPreviousYear) {
        this.outstandingPrincipalPreviousYear = outstandingPrincipalPreviousYear;
    }

    public BigDecimal getUnusedLimitPreviousYear() {
        return unusedLimitPreviousYear;
    }

    public void setUnusedLimitPreviousYear(BigDecimal unusedLimitPreviousYear) {
        this.unusedLimitPreviousYear = unusedLimitPreviousYear;
    }
}
