package nherlianto.psaketlboii;

import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;

@Controller
public class EtlController {
    public String home(Model model) {
        model.addAttribute("message", "Spring Boot + Thymeleaf rocks");
        return "home";
    }
}
