package nherlianto.psaketlboii;

import java.io.BufferedWriter;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class PsakEtlBoiiApplication implements CommandLineRunner {

    public static final Logger log = LoggerFactory.getLogger(PsakEtlBoiiApplication.class);

    public static void main(final String[] args) {
        SpringApplication.run(PsakEtlBoiiApplication.class, args);
    }

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void run(final String... strings) throws Exception {
        final String iNPUT_FILE = "./list_tanggal.csv";
        try (Reader reader = Files.newBufferedReader(Paths.get(iNPUT_FILE));
             CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);) {
            for (final CSVRecord csvRecord : csvParser) {
                final String date = csvRecord.get(0);
                final String libraryName = csvRecord.get(1);
                // String libraryNameBefore = csvRecord.get(2);
                System.out.println(date + " " + libraryName);
                //  generateFileDataHistory(date, libraryName);
        //       generateFileDpdHistory(date, libraryName);
//                generateFileCCFData(date, libraryName, libraryNameBefore);
            }
        }
    }

    public void generateFileDataHistory(final String appDate, final String libraryName) throws Exception {
        final ArrayList<DataHistory> listDataHistory = new ArrayList<DataHistory>();
        final String sqlDpdHistory =
                "SELECT 	" + appDate + " AS report_date,\n"
                        + "		ll.l0lnno AS no_rekening,\n"
                        + "		ll.l0name AS nama_debitur,  \n"
                        + "		ll.l0lnty AS fasilitas,\n"
                        + "		'' AS jenis_kredit,\n"
                        + "		ll.l0tyus AS jenis_penggunaan,\n"
                        + "		(lf.FCNWMT*-1)/100 AS plafond,\n"
                        + "		(ll.l0pamo*-1)/100 AS nominal,\n"
                        + "                CASE WHEN ll.l0cols IN (1,2) and l0lnty IN ('D/L', 'F/L') \n"
                        + "                     THEN \n"
                        + "                     (lf.FCNWMT*(-1) - ((ll.l0pamo*-1) - ll.l0papi))/100 \n"
                        + "                     ELSE 0 \n"
                        + "                END AS uncommitted,\n"
                        + "		0 AS bulan_lalu,\n"
                        + "		((ll.l0pamo*-1) - ll.l0papi)/100 AS outstanding,\n"
                        + "		(ll.l0duin)/100 AS bunga,\n"
                        + "		ll.l0cols AS kolektibilitas,\n"
                        + "		ifnull(dpd, 0) AS dpd,\n"
                        + "		ll.l0cirt AS rate_kontraktual,\n"
                        + "		ll.l0cycd AS currency,\n"
                        + "		ku.kurs AS kurs,\n"
                        + "		ll.L0STDT AS start_date,\n"
                        + "		ll.l0mtdt AS end_date\n"
                        + "	FROM " + libraryName + ".lloan ll LEFT JOIN \n"
                        + "	(SELECT hdlnno,\n"
                        + "		min(HDDTVL) AS dpd  \n"
                        + "		FROM " + libraryName + ".lhpdu \n"
                        + "		WHERE hdstat = 'A'\n"
                        + "		GROUP BY HDLNNO) dl \n"
                        + "	ON ll.l0lnno = dl.hdlnno\n"
                        + "	LEFT JOIN " + libraryName + ".lfacu lf ON (ll.l0csno = lf.FCCSNO AND ll.l0fcty = lf.fcfcty AND ll.l0fcsq = lf.fcfcsq)\n"
                        + "	LEFT JOIN " + libraryName + ".m4cu m4 ON (ll.L0CSNO = m4.cucode)\n"
                        + "         LEFT JOIN (SELECT kucyco, (kuamsl + kuamby)/2 AS kurs \n"
                        + "         FROM " + libraryName + ".a2ku \n"
                        + "         WHERE kutype = 'GL' and kudtvl  = (SELECT max(kudtvl) FROM " + libraryName + ".a2ku)\n"
                        + "         ORDER BY kudtvl desc) ku ON (ll.l0cycd = ku.kucyco)\n"
                        + "         WHERE ll.l0stat = 'A'\n"
                        + "UNION ALL\n"
                        + "SELECT  " + appDate + " AS report_date,\n"
                        + "		RKCODE AS no_rekening,\n"
                        + "		rename AS nama_debitur,   \n"
                        + "		mr.REATCO AS fasilitas,\n"
                        + "		'' AS jenis_kredit,\n"
                        + "		'1' AS jenis_penggunaan,\n"
                        + "		(lf.FCNWMT*(-1))/100 AS plafond,\n"
                        + "		(lf.FCNWMT*(-1))/100 AS nominal,\n"
                        + "                CASE WHEN mr.rdcoco IN (1,2) \n"
                        + "                THEN \n"
                        + "                 (lf.FCNWMT*(-1))/100 - CASE WHEN mr.reldbl < 0 THEN (mr.reldbl*(-1))/100 ELSE 0 END \n"
                        + "		ELSE 0 \n"
                        + "                END AS uncommitted,\n"
                        + "		0 AS bulan_lalu,\n"
                        + "		CASE WHEN mr.reldbl < 0 THEN (mr.reldbl*(-1))/100 ELSE 0 END AS outstanding,\n"
                        + "		(du2.bunga)/100 AS bunga,\n"
                        + "		mr.rdcoco AS kolektibilitas,\n"
                        + "		ifnull(dpd, 0) AS dpd,\n"
                        + "		rt.ARRAT1 AS rate_kontraktual,\n"
                        + "		mr.recyco AS currency,\n"
                        + "		ku.kurs AS kurs,\n"
                        + "		mr.RDDTBP AS start_date,\n"
                        + "		mr.RDDTEP AS end_date\n"
                        + "	from " + libraryName + ".m5reprk m5 LEFT JOIN\n"
                        + "	(SELECT DURECO,  \n"
                        + "		min(DUDTVL) AS dpd \n"
                        + "		FROM " + libraryName + ".H8DU \n"
                        + "		WHERE (DUTYPE='I' OR DUTYPE='O') AND DUSTAT='1'\n"
                        + "		GROUP BY dureco) du \n"
                        + "	ON m5.rkcode = du.dureco\n"
                        + "	LEFT JOIN " + libraryName + ".M5RE mr ON (m5.RKCODE = mr.RECODE)\n"
                        + "	LEFT JOIN  " + libraryName + ".lfacu lf ON (m5.RKCUCO = lf.FCCSNO AND m5.RKFTYP = lf.fcfcty AND m5.RKSEQU = lf.fcfcsq)\n"
                        + "	LEFT JOIN  " + libraryName + ".m4cu m4 ON (m5.RKCUCO = m4.cucode)\n"
                        + "	LEFT JOIN (SELECT dureco, sum(duamnt)*(-1) AS bunga FROM  " + libraryName + ".h8du WHERE DUTYPE = 'I' AND DUSTAT='1' GROUP BY dureco) du2 ON (m5.RKCODE = du2.dureco)\n"
                        + "       LEFT JOIN (SELECT MAX(h8.ARRECO) AS ARRECO, MAX(h8.ARRAT1) AS ARRAT1 FROM H8AR h8\n"
                        + "         WHERE h8.ARTYPE = 2\n"
                        + "         AND h8.ARDTVL = (SELECT MAX(ARDTVL) FROM " + libraryName + ".H8AR WHERE ARRECO=h8.ARRECO AND ARTYPE=2)\n"
                        + "         GROUP BY h8.ARRECO, h8.ARTYPE) rt ON (m5.RKCODE = rt.arreco)\n"
                        + "         LEFT JOIN (SELECT kucyco, (kuamsl + kuamby)/2 AS kurs \n"
                        + "         FROM " + libraryName + ".a2ku \n"
                        + "         WHERE kutype = 'GL' and kudtvl  = (SELECT max(kudtvl) FROM a2ku) \n"
                        + "         ORDER BY kudtvl desc) ku ON (mr.recyco = ku.kucyco)";

        jdbcTemplate.query(
                sqlDpdHistory,
                (rs, rowNum) -> new DataHistory(
                        rs.getString("report_date"),
                        rs.getString("no_rekening"),
                        rs.getString("nama_debitur"),
                        rs.getString("fasilitas"),
                        rs.getString("jenis_kredit"),
                        rs.getString("jenis_penggunaan"),
                        rs.getBigDecimal("plafond"),
                        rs.getBigDecimal("nominal"),
                        rs.getBigDecimal("uncommitted"),
                        rs.getBigDecimal("bulan_lalu"),
                        rs.getBigDecimal("outstanding"),
                        rs.getBigDecimal("bunga"),
                        rs.getInt("kolektibilitas"),
                        rs.getInt("dpd"),
                        rs.getBigDecimal("rate_kontraktual"),
                        rs.getString("currency"),
                        rs.getBigDecimal("kurs"),
                        rs.getString("start_date"),
                        rs.getString("end_date")
                )
        ).forEach(dataHistory -> listDataHistory.add(dataHistory));

        try (
                BufferedWriter writer = Files.newBufferedWriter(Paths.get("./data_history_" + appDate + ".csv"));
                CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                        .withHeader("report_date",
                                "no_rekening",
                                "nama_debitur",
                                "fasilitas",
                                "jenis_kredit",
                                "jenis_penggunaan",
                                "plafond",
                                "nominal",
                                "uncommitted",
                                "bulan_lalu",
                                "outstanding",
                                "bunga",
                                "kolektibilitas",
                                "dpd",
                                "rate_kontraktual",
                                "currency",
                                "kurs",
                                "start_date",
                                "end_date"
                        ));) {
            for (final DataHistory dataHistory : listDataHistory) {
                csvPrinter.printRecord(dataHistory.getReportDate(),
                        dataHistory.getContractNumber(),
                        dataHistory.getLoanHolder(),
                        dataHistory.getFacilityType(),
                        dataHistory.getCreditType(),
                        dataHistory.getTypeOfUse(),
                        dataHistory.getPlafondAmount(),
                        dataHistory.getPrincipalAmount(),
                        dataHistory.getUncommitedFacility(),
                        dataHistory.getPreviousOutstandingPrincipal(),
                        dataHistory.getOutstandingPrincipal(),
                        dataHistory.getInterestPastDue(),
                        dataHistory.getCollectibility(),
                        dataHistory.getDayPastDue(),
                        dataHistory.getInterestRate(),
                        dataHistory.getCurrency(),
                        dataHistory.getExcangeRate(),
                        dataHistory.getStartDate(),
                        dataHistory.getEndDate());
            }
            csvPrinter.flush();
        }

    }

    public void generateFileDpdHistory(final String appDate, final String libraryName) throws Exception {
        final ArrayList<DpdHistory> listDpdHistory = new ArrayList<DpdHistory>();
        final String sqlDpdHistory =
                "SELECT 'L' AS Jenis_Instrumen, "
                        + "     ll.l0lnno AS NomorLoan, "
                        + "     "+ appDate +" AS TanggalAkhirBulan,  "
                        + "     ll.l0cols AS KolekSistem, "
                        + "     ifnull(dpd, 0) AS JumlahHariTunggakan, "
                        + "     ll.l0lnty AS ProductCode, "
                        + "     ll.l0econ AS SektorEkonomiGroup, "
                        + "     ll.l0tyus AS JenisPenggunaan, "
                        + "     (lf.FCNWMT*-1)/100 AS Plafond, "
                        + "     m4.CUFLTY AS TipeNasabah,	 "
                        + "     ll.L0CYCD AS Currency, "
                        + "     ll.l0brcd AS Wilayah, "
                        + "     (((-1)*ll.L0PAMO) - ll.L0PAPI - ll.L0WOPI)/100 AS BakiDebet, "
                        + "	ll.L0MTDT AS TanggalJatuhTempo, "
                        + "     '' AS Bucket,"
                        + "     '' AS TanggalTunggakan,"
                        + "     ll.L0DUPI/100 AS TunggakanPokok,"
                        + "     ll.L0DUIN/100 AS TunggakanBunga,"
                        + "     ll.L0PAPI/100 AS PokokTerbayar,"
                        + "     ll.L0PAIN/100 AS BungaTerbayar,"
                        + "     "+ appDate +" AS Periode,"
                        + "     CASE WHEN ll.L0fcsq<10 THEN ll.l0csno || ll.l0fcty ||'0'||ll.l0fcsq ELSE ll.l0csno || ll.l0fcty || ll.l0fcsq  END AS FacilityCode,"
                        + "     ll.l0cirt AS Eir"
                        + " FROM "  + libraryName + ".lloan ll LEFT JOIN "
                        + " (SELECT hdlnno, "
                        + "	(days(TIMESTAMP_FORMAT('" + appDate + "', 'YYYYMMDD')) - days(TIMESTAMP_FORMAT(cast(min(HDDTVL) AS char(8)), 'YYYYMMDD'))) AS dpd  "
                        + "	FROM "  + libraryName + ".lhpdu "
                        + "	WHERE HDC1FL IN ('I','P') AND HDSTAT!='D'"
                        + "	GROUP BY HDLNNO) dl "
                        + " ON ll.l0lnno = dl.hdlnno "
                        + " LEFT JOIN  "  + libraryName + ".lfacu lf ON (ll.l0csno = lf.FCCSNO AND ll.l0fcty = lf.fcfcty AND ll.l0fcsq = lf.fcfcsq) "
                        + " LEFT JOIN  "  + libraryName + ".m4cu m4 ON (ll.L0CSNO = m4.cucode) "
                        + " WHERE ll.l0stat = 'A' "
                        + " UNION ALL "
                        + " SELECT  'P' AS Jenis_Instrumen, "
                        + "	m5.RKCODE AS NomorLoan, "
                        + "	"+ appDate +" AS TanggalAkhirBulan, "
                        + "	mr.RDCOCO AS KolekSistem, "
                        + "	ifnull(dpd, 0) as JumlahHariTunggakan, "
                        + "	mr.REATCO AS ProductCode, "
                        + "	mr.RDECCO AS SektorEkonomiGroup, "
                        + "	'' AS JenisPenggunaan, "
                        + "	(lf.FCNWMT*-1)/100 AS Plafond, "
                        + "	m4.CUFLTY AS TipeNasabah, "
                        + "	mr.RECYCO AS Currency, "
                        + "	mr.REBRCO AS Wilayah, "
                        + "            mr.RECLBL/100 AS BakiDebet,"
                        + "            mr.RDDTEP AS TanggalJatuhTempo, "
                        + "            '' AS Bucket, "
                        + "            '' AS TanggalTunggakan,"
                        + "            '' AS TunggakanPokok,"
                        + "            '' AS TunggakanBunga,"
                        + "            '' AS PokokTerbayar,"
                        + "            '' AS BungaTerbayar,"
                        + "            "+ appDate +" AS Periode,"
                        + "     	CASE WHEN lf.fcfcsq<10 THEN lf.fccsno || lf.fcfcty || '0' || lf.fcfcsq"
                        + "             ELSE lf.fccsno || lf.fcfcty || lf.FCFCSQ"		
                        + "             END AS FacilityCode, "
                        + "             e.ARRAT1 as Eir"
                        + "	from  "  + libraryName + ".m5reprk m5 LEFT JOIN "
                        + "	(SELECT DURECO,   "
                        + "		(days(TIMESTAMP_FORMAT('" + appDate + "', 'YYYYMMDD')) - days(TIMESTAMP_format(cast(min(DUDTVL) AS char(8)), 'YYYYMMDD'))) dpd "
                        + "		FROM  "  + libraryName + ".H8DU  "
                        + "		WHERE (DUTYPE='I' OR DUTYPE='P') AND DUSTAT='1' "
                        + "		GROUP BY dureco) du  "
                        + "	ON m5.rkcode = du.dureco "
                        + "	LEFT JOIN  "  + libraryName + ".M5RE mr ON (m5.RKCODE = mr.RECODE) "
                        + "	LEFT JOIN  "  + libraryName + ".lfacu lf ON (m5.RKCUCO = lf.FCCSNO AND m5.RKFTYP = lf.fcfcty AND m5.RKSEQU = lf.fcfcsq) "
                        + "	LEFT JOIN  "  + libraryName + ".m4cu m4 ON (m5.RKCUCO = m4.cucode)"
                        + "     LEFT JOIN  "
                        + "             (SELECT "
                        + "                     MAX(h8.ARRECO) AS ARRECO, "
                        + "                     MAX(h8.ARRAT1) AS ARRAT1 "
                        + "             FROM "
                        +                       libraryName + ".H8AR h8 "
                        + "             WHERE "
                        + "                     h8.ARTYPE = 2 "
                        + "                     AND h8.ARDTVL = ( "
                        + "                     SELECT "
                        + "                             MAX(ARDTVL) "
                        + "                             FROM "
                        +                               libraryName + ".H8AR "
                        + "                     WHERE "
                        + "                             ARRECO = h8.ARRECO "
                        + "                             AND ARTYPE = 2) "
                        + "                     GROUP BY h8.ARRECO, h8.ARTYPE) e on mr.RECODE = e.ARRECO";

        System.out.println(sqlDpdHistory);

        jdbcTemplate.query(
                sqlDpdHistory,
                (rs, rowNum) -> new DpdHistory(
                        rs.getString("Jenis_Instrumen"),
                        rs.getString("NomorLoan"),
                        rs.getString("TanggalAkhirBulan"),
                        rs.getString("KolekSistem"),
                        rs.getInt("JumlahHariTunggakan"),
                        rs.getString("ProductCode"),
                        rs.getString("SektorEkonomiGroup"),
                        rs.getString("JenisPenggunaan"),
                        rs.getBigDecimal("Plafond"),
                        rs.getString("TipeNasabah"),
                        rs.getString("Currency"),
                        rs.getString("Wilayah"),
                        rs.getBigDecimal("BakiDebet"),
                        rs.getString("TanggalJatuhTempo"),
                        rs.getString("Bucket"),
                        rs.getString("TanggalTunggakan"),
                        rs.getBigDecimal("TunggakanPokok"),
                        rs.getBigDecimal("TunggakanBunga"),
                        rs.getBigDecimal("PokokTerbayar"),
                        rs.getBigDecimal("BungaTerbayar"),
                        rs.getString("Periode"),
                        rs.getString("FacilityCode"),
                        rs.getBigDecimal("Eir")
                )
        ).forEach(dpdHistory -> listDpdHistory.add(dpdHistory));

        try (
                BufferedWriter writer = Files.newBufferedWriter(Paths.get("./dpd_history_" + appDate + ".csv"));
                CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                        .withHeader(
                                "Jenis_Instrumen",
                                "NomorLoan",
                                "TanggalAkhirBulan",
                                "KolekSistem",
                                "JumlahHariTunggakan",
                                "ProductCode",
                                "SektorEkonomiGroup",
                                "JenisPenggunaan",
                                "Plafond",
                                "TipeNasabah",
                                "Currency",
                                "Wilayah",
                                "BakiDebet",
                                "TanggalJatuhTempo",
                                "Bucket",
                                "TanggalTunggakan",
                                "TunggakanPokok",
                                "TunggakanBunga",
                                "PokokTerbayar",
                                "BungaTerbayar",
                                "Periode",
                                "FacilityCode",
                                "Eir"
                        ));) {
            for (final DpdHistory dpdHistory : listDpdHistory) {
                csvPrinter.printRecord(
                        dpdHistory.getInstrumentType(),
                        dpdHistory.getContractNumber(),
                        dpdHistory.getReportingDate(),
                        dpdHistory.getCollectibility(),
                        dpdHistory.getDayPastDue(),
                        dpdHistory.getProductType(),
                        dpdHistory.getEconomicSector(),
                        dpdHistory.getTypeOfUse(),
                        dpdHistory.getPlafond(),
                        dpdHistory.getCustomerType(),
                        dpdHistory.getCurrecny(),
                        dpdHistory.getGeographicLoaction(),
                        dpdHistory.getBakiDebet(),
                        dpdHistory.getEndDate(),
                        dpdHistory.getBucket(),
                        dpdHistory.getTanggalTunggakan(),
                        dpdHistory.getTunggakanPokok(),
                        dpdHistory.getTunggakanBunga(),
                        dpdHistory.getPokokTerbayar(),
                        dpdHistory.getBungaTerbayar(),
                        dpdHistory.getPeriode(),
                        dpdHistory.getFacilityCode(),
                        dpdHistory.getEir());
            }
            csvPrinter.flush();
        }
    }
}