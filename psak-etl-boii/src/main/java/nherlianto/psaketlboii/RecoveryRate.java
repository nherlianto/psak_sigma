package nherlianto.psaketlboii;

import java.math.BigDecimal;

public class RecoveryRate {
    private String instrumentType;
    private String productType;
    private String contractNumber;
    private String defaultDate;
    private String exposureWhenDefault;
    private BigDecimal eir;
    private String paymentDate;
    private BigDecimal paymentAmount;

    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getDefaultDate() {
        return defaultDate;
    }

    public void setDefaultDate(String defaultDate) {
        this.defaultDate = defaultDate;
    }

    public String getExposureWhenDefault() {
        return exposureWhenDefault;
    }

    public void setExposureWhenDefault(String exposureWhenDefault) {
        this.exposureWhenDefault = exposureWhenDefault;
    }

    public BigDecimal getEir() {
        return eir;
    }

    public void setEir(BigDecimal eir) {
        this.eir = eir;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
}
