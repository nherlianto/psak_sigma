package nherlianto.psaketlboii;

import java.math.BigDecimal;

public class DataHistory {
    private Long id;
    private String reportDate;
    private String contractNumber;
    private String loanHolder;
    private String facilityType;
    private String creditType;
    private String typeOfUse;
    private BigDecimal plafondAmount;
    private BigDecimal principalAmount;
    private BigDecimal uncommitedFacility;
    private BigDecimal previousOutstandingPrincipal;
    private BigDecimal outstandingPrincipal;
    private BigDecimal interestPastDue;
    private int collectibility;
    private int dayPastDue;
    private BigDecimal interestRate;
    private String currency;
    private BigDecimal excangeRate;
    private String startDate;
    private String endDate;   
    
    public DataHistory( String reportDate,
                        String contractNumber, 
                        String loanHolder,
                        String facilityType,
                        String creditType,
                        String typeOfUse,
                        BigDecimal plafondAmount,
                        BigDecimal principalAmount,
                        BigDecimal uncommitedFacility,
                        BigDecimal previousOutstandingPrincipal,
                        BigDecimal outstadingPrincipal,
                        BigDecimal interestPastDue,
                        int collectibility,
                        int dayPastDue,
                        BigDecimal interestRate,
                        String currency,
                        BigDecimal exchangeRate,
                        String startDate,
                        String endDate
                        ) {
        this.reportDate = reportDate;
        this.contractNumber = contractNumber;
        this.loanHolder = loanHolder;
        this.facilityType = facilityType;
        this.creditType = creditType;
        this.typeOfUse = typeOfUse;
        this.plafondAmount = plafondAmount;
        this.principalAmount = principalAmount;
        this.uncommitedFacility = uncommitedFacility;
        this.previousOutstandingPrincipal = previousOutstandingPrincipal;
        this.outstandingPrincipal = outstadingPrincipal;
        this.interestPastDue = interestPastDue;
        this.collectibility = collectibility;
        this.dayPastDue = dayPastDue;
        this.interestRate = interestRate;
        this.currency = currency;
        this.excangeRate = exchangeRate;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getLoanHolder() {
        return loanHolder;
    }

    public void setLoanHolder(String loanHolder) {
        this.loanHolder = loanHolder;
    }

    public String getFacilityType() {
        return facilityType;
    }

    public void setFacilityType(String facilityType) {
        this.facilityType = facilityType;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getTypeOfUse() {
        return typeOfUse;
    }

    public void setTypeOfUse(String typeOfUse) {
        this.typeOfUse = typeOfUse;
    }

    public BigDecimal getPlafondAmount() {
        return plafondAmount;
    }

    public void setPlafondAmount(BigDecimal plafondAmount) {
        this.plafondAmount = plafondAmount;
    }

    public BigDecimal getPrincipalAmount() {
        return principalAmount;
    }

    public void setPrincipalAmount(BigDecimal principalAmount) {
        this.principalAmount = principalAmount;
    }

    public BigDecimal getUncommitedFacility() {
        return uncommitedFacility;
    }

    public void setUncommitedFacility(BigDecimal uncommitedFacility) {
        this.uncommitedFacility = uncommitedFacility;
    }

    public BigDecimal getPreviousOutstandingPrincipal() {
        return previousOutstandingPrincipal;
    }

    public void setPreviousOutstandingPrincipal(BigDecimal previousOutstandingPrincipal) {
        this.previousOutstandingPrincipal = previousOutstandingPrincipal;
    }

    public BigDecimal getOutstandingPrincipal() {
        return outstandingPrincipal;
    }

    public void setOutstandingPrincipal(BigDecimal outstandingPrincipal) {
        this.outstandingPrincipal = outstandingPrincipal;
    }

    public BigDecimal getInterestPastDue() {
        return interestPastDue;
    }

    public void setInterestPastDue(BigDecimal interestPastDue) {
        this.interestPastDue = interestPastDue;
    }

    public int getCollectibility() {
        return collectibility;
    }

    public void setCollectibility(int collectibility) {
        this.collectibility = collectibility;
    }

    public int getDayPastDue() {
        return dayPastDue;
    }

    public void setDayPastDue(int dayPastDue) {
        this.dayPastDue = dayPastDue;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getExcangeRate() {
        return excangeRate;
    }

    public void setExcangeRate(BigDecimal excangeRate) {
        this.excangeRate = excangeRate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

}
