package nherlianto.psaketlboii;

import java.math.BigDecimal;

class DpdHistory {
    private String instrumentType;
    private String contractNumber;
    private String reportingDate;
    private String collectibility;
    private int dayPastDue;
    private String productType;
    private String economicSector;
    private String typeOfUse;
    private BigDecimal plafond;
    private String customerType;
    private String currecny;
    private String geographicLoaction;
    private BigDecimal bakiDebet;
    private String endDate;
    private String bucket;
    private String tanggalTunggakan;
    private BigDecimal tunggakanPokok;
    private BigDecimal tunggakanBunga;
    private BigDecimal pokokTerbayar;
    private BigDecimal bungaTerbayar;
    private String periode;
    private String facilityCode;
    private BigDecimal eir;

    public DpdHistory(
            final String instrumentType,
            final String contractNumber,
            final String reportingDate,
            final String collectibility,
            final int dayPastDue,
            final String productType,
            final String economicSector,
            final String typeOfUse,
            final BigDecimal plafond,
            final String customerType,
            final String currency,
            final String geographicLoaction,
            final BigDecimal bakiDebet,
            final String endDate,
            final String bucket,
            final String tanggalTunggakan,
            final BigDecimal tunggakanPokok,
            final BigDecimal tunggakanBunga,
            final BigDecimal pokokTerbayar,
            final BigDecimal bungaTerbayar,
            final String periode,
            final String facilityCode,
            final BigDecimal eir) {
        this.instrumentType = instrumentType;
        this.contractNumber = contractNumber;
        this.reportingDate = reportingDate;
        this.collectibility = collectibility;
        this.dayPastDue = dayPastDue;
        this.productType = productType;
        this.economicSector = economicSector;
        this.typeOfUse = typeOfUse;
        this.plafond = plafond;
        this.customerType = customerType;
        this.currecny = currency;
        this.geographicLoaction = geographicLoaction;
	    this.bakiDebet = bakiDebet;
        this.endDate = endDate;
        this.bucket = bucket;
        this.tanggalTunggakan = tanggalTunggakan;
	    this.tunggakanPokok = tunggakanPokok;
	    this.tunggakanBunga = tunggakanBunga;
	    this.pokokTerbayar = pokokTerbayar;
	    this.bungaTerbayar = bungaTerbayar;
	    this.periode = periode;
        this.facilityCode = facilityCode;
        this.eir = eir;
    }

    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(final String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(final String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getReportingDate() {
        return reportingDate;
    }

    public void setReportingDate(final String reportingDate) {
        this.reportingDate = reportingDate;
    }

    public String getCollectibility() {
        return collectibility;
    }

    public void setCollectibility(final String collectibility) {
        this.collectibility = collectibility;
    }

    public int getDayPastDue() {
        return dayPastDue;
    }

    public void setDayPastDue(final int dayPastDue) {
        this.dayPastDue = dayPastDue;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(final String productType) {
        this.productType = productType;
    }

    public String getEconomicSector() {
        return economicSector;
    }

    public void setEconomicSector(final String economicSector) {
        this.economicSector = economicSector;
    }

    public String getTypeOfUse() {
        return typeOfUse;
    }

    public void setTypeOfUse(final String typeOfUse) {
        this.typeOfUse = typeOfUse;
    }

    public BigDecimal getPlafond() {
        return plafond;
    }

    public void setPlafond(final BigDecimal plafond) {
        this.plafond = plafond;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(final String customerType) {
        this.customerType = customerType;
    }

    public String getCurrecny() {
        return currecny;
    }

    public void setCurrecny(final String currecny) {
        this.currecny = currecny;
    }

    public String getGeographicLoaction() {
        return geographicLoaction;
    }

    public void setGeographicLoaction(final String geographicLoaction) {
        this.geographicLoaction = geographicLoaction;
    }

    public BigDecimal getBakiDebet() {
        return bakiDebet;
    }

    public void setBakiDebet(final BigDecimal bakiDebet) {
        this.bakiDebet = bakiDebet;
    }

   public String getEndDate() {
        return endDate;
    }

    public void setEndDate(final String endDate) {
        this.endDate = endDate;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(final String bucket) {
        this.bucket = bucket;
    }

    public String getTanggalTunggakan() {
        return tanggalTunggakan;
    }

    public void setTanggalTunggakan(final String tanggalTunggakan) {
        this.tanggalTunggakan = tanggalTunggakan;
    }

    public BigDecimal getTunggakanPokok() {
        return tunggakanPokok;
    }

    public void setTunggakanPokok(final BigDecimal tunggakanPokok) {
        this.tunggakanPokok = tunggakanPokok;
    }

    public BigDecimal getTunggakanBunga() {
        return tunggakanBunga;
    }

    public void setTunggakanBunga(final BigDecimal tunggakanBunga) {
        this.tunggakanBunga = tunggakanBunga;
    }

    public BigDecimal getPokokTerbayar() {
        return pokokTerbayar;
    }

    public void setPokokTerbayar(final BigDecimal pokokTerbayar) {
        this.pokokTerbayar = pokokTerbayar;
    }

    public BigDecimal getBungaTerbayar() {
        return bungaTerbayar;
    }

    public void setBungaTerbayar(final BigDecimal bungaTerbayar) {
        this.bungaTerbayar = bungaTerbayar;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(final String periode) {
        this.periode = periode;
    }

    public String getFacilityCode() {
        return facilityCode;
    }
    
    public void setFacilityCode(final String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public BigDecimal getEir() {
        return eir;
    }
    
    public void setEir(final BigDecimal eir) {
        this.eir = eir;
    }
}