-- cek selisih unused facility di database staging
select * from 
(select oc.acc_no, iaf.UNUSED_AMOUNT, oc.UNUSED_FACILITY_AMOUNT, iaf.UNUSED_AMOUNT - oc.UNUSED_FACILITY_AMOUNT as selisih_unused_facility
from output_ckpn oc left join input_acc_facility  iaf on (oc.acc_no = iaf.LOAN_ACC_NO )) temp_facility 
where selisih_unused_facility > 0;
